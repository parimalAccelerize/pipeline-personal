#!/bin/bash
DEPLOY_DIR="force-app/main/default"
TEST_LEVEL="NoTestRun"

# If DEPLOY_MODE = Manifest use manifest file
# If DEPLOY_MODE = Diff take the diff of 2 commits
if [ $1 == 'Manifest' -o $1 == '-m' ]
then
    # Move manifest to deploy folder
    mv manifest/package.xml $DEPLOY_DIR
    # Deploy to target org
    sfdx force:source:deploy -w 10 -x $DEPLOY_DIR/package.xml -u TargetOrg -l $TEST_LEVEL;
else
    # Deploy the changed components within DEPLOY_DIR to target org and run unit tests
    STARTCOMMIT=$2
    ENDCOMMIT=$3
    if [ -z "$STARTCOMMIT" ]
    then
        STARTCOMMIT="HEAD~1"
    fi
    if [ -z "$ENDCOMMIT" ]
    then
        ENDCOMMIT="HEAD"
    fi
    git diff $STARTCOMMIT $ENDCOMMIT --name-only --diff-filter=ACM $DEPLOY_DIR | tr '\n' , | sed '$s/,$//' | xargs -I{} sfdx force:source:deploy -w 10 -u TargetOrg -l $TEST_LEVEL -p {};
fi